%% clear
clear all global
restoredefaultpath

%% add the path to th_ptb
addpath('/home/th/git/th_ptb/') % change this to where th_ptb is on your system

%% initialize the PTB
th_ptb.init_ptb('/home/th/git_other/Psychtoolbox-3/'); % change this to where PTB is on your system

%% get a configuration object
ptb_cfg = th_ptb.PTB_Config();

%% do the configuration
ptb_cfg.fullscreen = false;
ptb_cfg.window_scale = 0.2;
ptb_cfg.skip_sync_test = true;
ptb_cfg.hide_mouse = false;

%% get th_ptb.PTB object
ptb = th_ptb.PTB.get_instance(ptb_cfg);

%% init audio and triggers
ptb.setup_screen;
ptb.setup_audio;
ptb.setup_trigger;

%% load sound
my_sound = th_ptb.stimuli.auditory.Wav('da_40.wav');

%% prepare sound
ptb.prepare_audio(my_sound);

%% schedule sound
ptb.schedule_audio;

%% play at once
ptb.play_without_flip;

%% play with 500ms delay
ptb.prepare_audio(my_sound, 0.5);
ptb.schedule_audio;
ptb.play_without_flip;

%% play two sounds in a row
ptb.prepare_audio(my_sound);
ptb.prepare_audio(my_sound, 0.6, true);

ptb.schedule_audio;
ptb.play_without_flip;

%% create a sine wave and make a sound object
s_rate = 44100;
freq = 440;
amplitude = 0.1;
duration = 1;

sound_data = amplitude * sin(2*pi*(1:(s_rate*duration))/s_rate*freq);

sin_sound = th_ptb.stimuli.auditory.FromMatrix(sound_data, s_rate);

%% play it
ptb.prepare_audio(sin_sound);
ptb.schedule_audio;
ptb.play_without_flip;

%% play a sound with trigger
ptb.prepare_audio(my_sound);
ptb.prepare_trigger(1);

ptb.schedule_audio;
ptb.schedule_trigger;

ptb.play_without_flip;

%% play a multiple sounds with triggers
ptb.prepare_audio(my_sound);
ptb.prepare_trigger(1);

ptb.prepare_audio(my_sound, 0.5, true);
ptb.prepare_trigger(2, 0.5, true);

ptb.schedule_audio;
ptb.schedule_trigger;

ptb.play_without_flip;

%% play a multiple sounds with triggers on flip
hello_world = th_ptb.stimuli.visual.Text('Hello World!');
ptb.draw(hello_world);

ptb.prepare_audio(my_sound);
ptb.prepare_trigger(1);

ptb.prepare_audio(my_sound, 0.5, true);
ptb.prepare_trigger(2, 0.5, true);

ptb.schedule_audio;
ptb.schedule_trigger;

ptb.play_on_flip;

ptb.flip(GetSecs + 1);